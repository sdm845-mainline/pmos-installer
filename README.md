# pmOS-installer

A relatively nice postmarketOS installer for distributing pmOS to end users.

## UPDATING THE KERNEL ON A POSTMARKETOS INSTALL MADE USING THIS INSTALLER WILL MAKE YOUR DEVICE NO LONGER BOOT

Until we either merge the features of the custom initramfs used by this installer into postmarketos-mkinitfs, or add a custom hook to reuse the initramfs, updating the kernel will flash a new boot image using the regular postmarketOS initramfs which does not know how to mount the rootfs, the device will boot to the postmarketOS splash screen and get stuck requiring your to generate a new boot image and flash it.

## Cloning

Make sure you include the initrd submodule when cloning:
```sh
git clone https://gitlab.com/sdm845-mainline/pmos-installer installer --recurse-submodules
```

The script `makeinstaller.sh` will generate a flashable zip that can be flashed from within TWRP to install postmarketOS on your device by placing a rootfs on the userdata partition. It will only ever flash the `boot` and `dtbo` partitions.
The rootfs will be placed in `/data/.stowaways/pmos_root.img`

Run `./makeinstaller.sh -h` for options, it is possible to create a minimal flashable zip (compatible with TWRP) that also allows users to select which boot slot to flash to.

The installer needs to know which partition is the userdata partition, you can find this by running `ls -la /dev/block/bootdevice/by-name/userdata` from TWRP or Android and looking at the symlink, e.g.

```sh
OnePlus6:/ # ls -la /dev/block/bootdevice/by-name/userdata
lrwxrwxrwx 1 root root 16 1970-03-20 14:18 /dev/block/bootdevice/by-name/userdata -> /dev/block/sda17
```

This is because the rootfs will be placed on this partition, so the ramdisk has to know what partition to mount to find the rootfs.

## Issues

* The installer doesn't currently support QCDT devices

## Running the script

Before generating an installer, you have to build postmarketos. A split install is required as we only want the rootfs, not the extra boot partition.
```
pmbootstrap install --split
# Export to make it available to the script
pmbootstrap export
```

Now generate the installer with
```
# Replace 'PARTITION' with the partition you got earlier, e.g. 'sda17'
./makeinstaller.sh -p PARTITION
```

> **NOTE:** You can alternatively specify a ramdisk to use, this will make the partition optional

## Parameters

Parameters are set in the output zip file name, for example: `postmarketOS-20200806-enchilada-phosh-SLOT_a.zip`.

### SLOT_x (*optional*)

Selects which slot the boot image should be flashed to. Valid values are:

* `a` to flash to the A slot (`/dev/block/bootdevice/by-name/boot_a`).
* `b` to flash to the B slot (`/dev/block/bootdevice/by-name/boot_b`).
* Any other value to flash to the current slot (`/dev/block/bootdevice/by-name/boot`).

The values are case sensitive (and must be lower case).

If your device does not use slots, do not include this parameter.
